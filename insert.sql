INSERT INTO ecc_db.dbo.branches (bank_code, code, created_by, created_date, modified_date, modified_by, enabled, name, routing_number) VALUES (1, 1, 'admin', CONVERT(BIGINT, CONVERT(VARCHAR(8), GETDATE(), 112)), NULL, NULL, 1, 'Cairo', NULL);


INSERT INTO ecc_db.dbo.banks (code, created_by, created_date, modified_date, modified_by, enabled, name, primary_routing_number) VALUES ( 1, 'admin', CONVERT(BIGINT, CONVERT(VARCHAR(8), GETDATE(), 112)), NULL, NULL, 1, 'Central Bank of Egypt', NULL);
